<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('departments')->insert([
            'department_name' => 'ADMIN'
        ]);
        
    	DB::table('positions')->insert([
            'department_id' => 1,
            'position_name' => 'ADMIN'
        ]);

    	DB::table('permission')->insert([
            'position_id' => '1',
            'upload' => 1,
            'delete' => 1,
            'download' => 1,
        ]);
        
    	DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@example.com',
            'password' => bcrypt('secret'),
            'api_token' => Str::random(60),
            'position_id' => 1
        ]);
    }
}
