@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header h2">
            Users           

            <div class="float-right d-flex">
                <new-users-component :user-auth="{{Auth::user()}}"></new-users-component>
            </div>
        </div>

        <div class="card-body">
            <users-component :user-auth="{{Auth::user()}}"></users-components>
        </div>
    </div>
</div>
@endsection
