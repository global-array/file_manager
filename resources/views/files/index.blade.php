@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header h2">
            Files Management

            <div class="float-right d-flex">
                @if(Auth::user()->position->permission->upload != 0)
                <new-files-component :user-auth="{{Auth::user()}}"></new-files-component>
                @else
                @endif
            </div>
        </div>

        <div class="card-body">
            <files-component :user-auth="{{Auth::user()}}"></files-component>
        </div>
    </div>
</div>
@endsection
