@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header h2">
            Departments

            <div class="float-right d-flex">
                <new-departments-component :user-auth="{{Auth::user()}}"></new-departments-component>
            </div>
        </div>

        <div class="card-body">
            <departments-component :user-auth="{{Auth::user()}}"></departments-component>
        </div>
    </div>
</div>
@endsection
