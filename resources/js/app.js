/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
locale.use(lang);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
import store from './vuex/store';
Vue.component('users-component', require('./components/users/UsersComponent.vue').default);
Vue.component('new-users-component', require('./components/users/NewUserComponent.vue').default);
Vue.component('files-component', require('./components/files/FileComponent.vue').default);
Vue.component('new-files-component', require('./components/files/NewFileComponent.vue').default);
Vue.component('departments-component', require('./components/departments/DepartmentComponent.vue').default);
Vue.component('new-departments-component', require('./components/departments/NewDepartmentComponent.vue').default);

Vue.use(ElementUI);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});
