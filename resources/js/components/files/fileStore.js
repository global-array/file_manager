import axios from 'axios'

const state = () => {
    return {
        files: [],
        folders: []
    };
};

const getters = {
    allFiles: state => state.files,
    allFolders: state => state.folders,
};

const actions = {
    async fetchFiles({ commit }, files){
        const response = await axios.get('/api/files/'+files.folder_id+'/'+files.type, files.config)
        
        commit('setFiles', response.data)

        return response
    },

    async fetchFolders({commit}, folders){
        const response = await axios.get('/api/folders/'+folders.id, folders.config)

        commit('setFolders', response.data)

        return response
    },

    async shareFolder({commit}, folders){
        const response = await axios.put('/api/folders/share', folders.data, folders.config)

        return response
    },

    async addFolder({commit}, folder){
        const response = await axios.post('/api/folders',folder.data, folder.config)

        commit('newFolder', response.data.data)

        return response.data
    },

    async downloadFile({commit}, files){
        const response = await axios.get('/api/files/download/'+files.id, files.config)

        return response
    }
};

const mutations = {
    setFiles: (state, files) => (state.files = files),
    setFolders: (state, folders) => (state.folders = folders),
    newFolder: (state, folders) => state.folders[0].folders.push(folders),
    // newFolder: (state, folders) => {
    //     const index = state.folders.findIndex(department => department.id === dltDepartment.id);
    //     if (index !== -1) {
    //         state.departments.splice(index, 1);
    //     }
    // }
};

export default {
    state,
    getters,
    actions,
    mutations
};