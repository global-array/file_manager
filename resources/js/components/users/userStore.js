import axios from 'axios'

const state = () => {
    return {
        users: [],
    };
};

const getters = {
    allUsers: state => state.users,
};

const actions = {
    async fetchUsers({ commit }, config){
        const response = await axios.get('/api/users', config)
        
        commit('setUsers', response.data)
    },

    async addUser({commit}, data){
        const response = await axios.post('/api/users', data.values, data.config)

        commit('newUser', response.data.data)

        return response.data
    }
};

const mutations = {
    setUsers: (state, users) => (state.users = users),
    newUser: (state, user) => state.users.push(user),
};

export default {
    state,
    getters,
    actions,
    mutations
};