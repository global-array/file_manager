import axios from 'axios'

const state = () => {
    return {
        departments: [],
        positions: [],
        permisions: [],
    };
};

const getters = {
    allDepartments: state => state.departments,
    positionPermission: state => state.permisions,
    positionByDepartment: state => state.positions,
};

const actions = {
    async fetchDepartments({ commit }, config){
        const response = await axios.get('/api/departments', config)
        
        commit('setDepartments', response.data)
    },

    async fetchPositions({commit}, data){
        const response = await axios.get('/api/departments/positions/'+data.id, data.config)

        commit('setPositions', response.data)

        return response.data
    },

    async fetchPermission({ commit }, data){
        const response = await axios.get('/api/departments/positions/permission/'+data.id, data.config)
        
        // commit('setPermission', response.data)

        return response.data
    },

    async addDepartment({ commit }, department){
        const response = await axios.post('/api/departments', department.data, department.config)

        commit('newDepartment', response.data.data)

        return response.data;
    },

    async addPermission({ commit }, permission){
        const response = await axios.post('/api/departments/positions/permission', permission.data, permission.config)

        // commit('setPermission', reponse.data)

        return response
    },

    async addPosition({ commit }, position){
        const response = await axios.post('/api/departments/positions', position.data, position.config)

        commit('newPosition', response.data.data)

        return response.data;
    },
    
    async removeDepartment({ commit }, department){
        const response = await axios.delete('/api/departments/'+department.id, department.config)

        commit('deleteBank', response.data.data)

        return response.data;
    }
};

const mutations = {
    setDepartments: (state, departments) => (state.departments = departments),
    setPositions: (state, positions) => (state.positions = positions),
    setPermission: (state, permission) => (state.permisions = permission),
    newPosition: (state, positions) => state.positions.push(positions),
    newDepartment: (state, department) => state.departments.push(department),
    deleteDepartment: (state, dltDepartment) => {
        const index = state.departments.findIndex(department => department.id === dltDepartment.id);
        if (index !== -1) {
            state.departments.splice(index, 1);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};