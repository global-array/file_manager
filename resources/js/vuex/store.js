import Vue from 'vue';
import Vuex from 'vuex';

import departmentStore from './../components/departments/departmentStore';
import fileStore from './../components/files/fileStore';
import userStore from './../components/users/userStore';

Vue.use(Vuex);
Vue.config.debug = true;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        departmentStore,
        fileStore,
        userStore
    },
    strict: debug
});