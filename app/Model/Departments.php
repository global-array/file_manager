<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departments extends Model
{
	use SoftDeletes;
	
	protected $table = 'departments';
	public $primaryKey = 'id';
	public $timestamps = true;

	protected $dates = ['deleted_at'];

	function positions(){
		return $this->hasMany('App\Model\Positions', 'department_id');
	}
}
