<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shared_folders extends Model
{
	protected $table = 'shared_folders';
	public $primaryKey = 'id';
	public $timestamps = true;

	
	public function folders(){
		return $this->hasOne('App\Model\Folders', 'id', 'folder_id');
	}
}
