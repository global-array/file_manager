<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Uploads extends Model
{
	protected $table = 'uploads';
	public $primaryKey = 'id';
	public $timestamps = true;

	protected $fillable = [
        'file_name',  'file_type', 'department_name', 'extension', 'uploaded_by', 'folder_id'
	];
	
	
	public function folder(){
		return $this->hasOne('App\Model\Folders', 'id', 'folder_id');
	}
}
