<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
	protected $table = 'permission';
	public $primaryKey = 'id';
	public $timestamps = true;

	public function position(){
		return $this->belongsTo('App\Model\Positions');
	}
}
