<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Folders extends Model
{
	protected $table = 'folder';
	public $primaryKey = 'id';
	public $timestamps = true;
	
	public function files(){
		return $this->hasMany('App\Model\Uploads', 'folder_id', 'id');
	}

	public function shared(){
		return $this->belongsTo('App\Model\Shared_folders', 'id', 'folder_id');
	}
}
