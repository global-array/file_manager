<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Positions extends Model
{
	use SoftDeletes;

	protected $table = 'positions';
	public $primaryKey = 'id';
	public $timestamps = true;

	protected $dates = ['deleted_at'];

	public function department(){
		return $this->hasOne('App\Model\Departments', 'id', 'department_id');
	}

	public function permission(){
		return $this->hasOne('App\Model\Permissions', 'position_id', 'id');
	}
}
