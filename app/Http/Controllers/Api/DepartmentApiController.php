<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Departments;
use App\Model\Permissions;
use App\Model\Positions;

class DepartmentApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function getDepartments(){
        $data = Departments::with('positions')->get();
        return response()->json($data);
    }

    function addDepartment(Request $request){
        $input = $request->all();
        $department = new Departments;
        $department->department_name = strtoupper($input['department_name']);
        if($department->save()){
            return response()->json([
                'data' => $department,
                'status' => 201,
                'message' => 'New Department has been added'
            ]);
        }
    }

    function positionsByDepartment($id){
        return Positions::where('department_id', $id)->get();
    }

    function addPosition(Request $request){
        $input = $request->all();
        $position = new Positions;
        $position->department_id = $input['department_id'];
        $position->position_name = strtoupper($input['position_name']);
        if($position->save()){
            $setup = new Permissions;
            $setup->position_id = $position->id;
            $setup->save();
            return response()->json([
                'data' => $position,
                'status' => 201,
                'message' => 'New Position has been added'
            ]);
        }
    }

    function getPermission($id){
        $permission = Permissions::where('position_id', $id)->first();
        return response()->json($permission);
    }

    function setPermission(Request $request){
        $input = $request->all();
        $checkPermission = Permissions::find($input['id']);
        if($checkPermission){
            if($input['permissions']['upload']){
                $checkPermission->upload = 1;
            }else{
                $checkPermission->upload = 0;
            }
            // if($input['permissions']['view']){
            //     $checkPermission->view = 1;
            // }else{
            //     $checkPermission->view = 0;
            // }
            if($input['permissions']['download']){
                $checkPermission->download = 1;
            }else{
                $checkPermission->download = 0;
            }
            if($input['permissions']['delete']){
                $checkPermission->delete = 1;
            }else{
                $checkPermission->delete = 0;
            }
            if($checkPermission->save()){
                return response()->json([
                    'data' => $checkPermission,
                    'status' => 201,
                    'message' => 'Permission has been set.'
                ]);
            }
        }else{
            $permission = new Permissions;
            $permission->position_id = $input['id'];
            if($input['permissions']['upload']){
                $permission->upload = 1;
            }else{
                $permission->upload = 0;
            }
            if($input['permissions']['view']){
                $permission->view = 1;
            }else{
                $permission->view = 0;
            }
            if($input['permissions']['download']){
                $permission->download = 1;
            }else{
                $permission->download = 0;
            }
            if($input['permissions']['delete']){
                $permission->delete = 1;
            }else{
                $permission->delete = 0;
            }
            if($permission->save()){
                return response()->json([
                    'data' => $permission,
                    'status' => 201,
                    'message' => 'Permission has been set.'
                ]);
            }
        }
        
    }
}
