<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\Folders;
use App\Model\Shared_folders;
use User;

class FoldersApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    function getFolders($id){
        $folders = Folders::where('added_by', $id)->get();
        $shares = Auth::user()->shared_folder;
        // $shared_folders = Shared_folders::where('shared_to', Auth::id())->with('folders')->get();
        if(Auth::id() != 1){
            $shared_folders = array();
            foreach($shares as $share){
                array_push($shared_folders, Folders::find($share->folder_id)) ;
            };
        }else{
            $shared_folders = Folders::where('added_by', '!=', 1)->get();
        }
        return response()->json([[
                'type' => 'Private',
                'folders' => $folders
            ],[
                'type' => 'Shared',
                'folders' => $shared_folders
            ]
        ]);
    }

    function shareFolder(Request $request){
        $input = $request->all();
        $shared = new Shared_folders;
        $shared->folder_id = $input['id'];
        $shared->shared_to = $input['user_id'];
        $shared->shared_by = Auth::id();
        if($shared->save()){
            return $shared;
        }
    }

    function addFolder(Request $request){
        $input = $request->all();
        $folder = new Folders;
        $folder->folder_name = $input['folder_name'];
        $folder->added_by = Auth::id();
        if($folder->save()){
            return response()->json([
                'message' => 'New Folder has been added.',
                'data' => $folder
            ], 201);
        }

        return response()->json(false);
    }
}
