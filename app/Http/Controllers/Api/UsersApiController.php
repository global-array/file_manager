<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\User;

class UsersApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getUsers(){
        $data = User::where('id', '!=', Auth::id())->where('id', '!=', 1)->with('position', 'position.department')->get();
        return response()->json($data);
    }

    function newUser(Request $request){
        $input = $request->all();
        $user = new User;
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = bcrypt('secret');
        $user->position_id = $input['position'];
        $user->api_token = Str::random(60);
        if($user->save()){
            return response()->json([
                'data' => $user,
                'message' => 'New User has been added.',
                'status' => 201
            ]);
        }
    }
}
