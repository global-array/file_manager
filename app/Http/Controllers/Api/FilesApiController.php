<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Model\Folders;
use App\Model\Uploads;

class FilesApiController extends Controller
{
    
    private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    private $audio_ext = ['mp3', 'ogg', 'mpga'];
    private $video_ext = ['mp4', 'mpeg'];
    private $document_ext = ['doc', 'docx', 'pdf', 'odt', 'ppt', 'csv'];
    private $compressed_ext = ['zip', 'rar'];
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    function getFiles($folder_id, $type){
        // $row = Folders::where('id', $folder_id)->with('files')->first();
        // return $row->files[0]->department_name;
        $dept = Auth::user()->position->department['department_name'];
        // $dept = $row->files[0]->department_name;
        $files = Uploads::where('department_name', $dept)->where('file_type', $type)->where('folder_id', $folder_id)->with('folder')->get();
        // $files = Uploads::where('file_type', $type)->where('folder_id', $folder_id)->with('folder')->get();
        if(Auth::user()->position_id == 1){
            return Uploads::where('file_type', $type)->where('folder_id', $folder_id)->with('folder')->get();
        }
        return $files;
    }

    function download($id){
        $file = uploads::find($id);
        // return Storage::download($file->file_name);
        // return Storage::get(storage_path('public/'.$file->department_name.'/'.$file->file_type.'/'.$file->file_name));
        // return storage_path();
        // return Storage::download("app/public/storage/{$file->department_name}/{$file->file_type}/{$file->file_name}");
        // return response()->download(storage_path("app/public/{$file->department_name}/{$file->file_type}/{$file->file_name}"));
        return response()->download('/uploads/'.$file->department_name.'/'.$file->file_type.'/'.$file->file_name);
    }

    function uploads(Request $request){
        $uploadId = [];
        if($request->folder_id == ''){
            return response()->json([
                'message' => 'Please Select a folder.'
            ], 404);
        }
        if( $files = $request->file('file')) {
            foreach($request->file('file') as $key => $file){
                $name = strtolower($file->getClientOriginalName());
                $extension = strtolower($file->getClientOriginalExtension());
                $type = $this->getType($extension);
                
                $folder_id = Folders::find($request->folder_id);
                $dept = Auth::user()->position->department['department_name'];
                $checkFile = Uploads::where('file_name', $name)->first();
                if($checkFile){
                    return response()->json([
                        'message' => 'file already exist'
                    ], 400);
                }
                $model = new Uploads();
                
                // if (Storage::putFileAs('/public/' . $dept . '/' . $type . '/', $file, $name . '.' . $extension)) {
                    
                if ($file->move('uploads/' . $dept . '/' . $folder_id->folder_name . '/' . $type . '/', $name)) {
                    $model->file_name = $name;
                    $model->file_type = $type;
                    $model->extension = $extension;
                    $model->department_name = Auth::user()->position->department['department_name'];
                    $model->status = 0;
                    $model->uploaded_by = Auth::id();
                    $model->folder_id = $folder_id->id;
                    $model->save();
                    // $model::create([
                    //     'file_name' => $name,
                    //     'file_type' => $type,
                    //     'extension' => $extension,
                    //     'department_name' => Auth::user()->position->department['department_name'],
                    //     'status' => 0,
                    //     'uploaded_by' => Auth::id(),
                    //     'folder_id' => $folder_id->id
                    // ]);
                }
            }
        }
        return response()->json(false);
    }

    private function getType($ext)
    {
        if (in_array($ext, $this->image_ext)) {
            return 'image';
        }

        if (in_array($ext, $this->audio_ext)) {
            return 'audio';
        }

        if (in_array($ext, $this->video_ext)) {
            return 'video';
        }

        if (in_array($ext, $this->document_ext)) {
            return 'document';
        }

        if(in_array($ext, $this->compressed_ext)) {
            return 'compressed';
        }
    }
}
