<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'departments'], function(){
    Route::get('/positions/permission/{id}', 'Api\DepartmentApiController@getPermission');
    Route::post('/positions/permission', 'Api\DepartmentApiController@setPermission');
    Route::get('/positions/{id}', 'Api\DepartmentApiController@positionsByDepartment');
    Route::post('/positions', 'Api\DepartmentApiController@addPosition');
    Route::post('/', 'Api\DepartmentApiController@addDepartment');
    Route::get('/', 'Api\DepartmentApiController@getDepartments');
});

Route::group(['prefix' => 'users'], function(){
    Route::post('/', 'Api\UsersApiController@newUser');
    Route::get('/', 'Api\UsersApiController@getUsers');
});

Route::group(['prefix' => 'files'], function(){
    Route::post('/uploads', 'Api\FilesApiController@uploads');
    Route::get('/download/{id}', 'Api\FilesApiController@download');
    Route::get('/{folder_id}/{type}', 'Api\FilesApiController@getFiles');
});

Route::group(['prefix' => 'folders'], function(){
    Route::put('/share', 'Api\FoldersApiController@shareFolder');
    Route::get('/{id}', 'Api\FoldersApiController@getFolders');
    Route::post('/', 'Api\FoldersApiController@addFolder');
});